import express, { json } from 'express';
import { createHash } from 'crypto';
import pgClient from './util/pgClient';

// Configuration
const app = express();
app.use(express.static('./public'));
app.use(express.json());

class Login {
    name!: string;
    password!: string;
}

class Response {
    status: number;
    message: string;
    constructor(status: number, message: string) {
        this.status = status;
        this.message = message;
    }
}

app.post('/auth/login', async (req, res) => {
    const reqLogin: Login = req.body;
    if (reqLogin.name) {
        const queryRes = await pgClient.query(
            'SELECT DISTINCT * FROM login WHERE name = $1 AND password = CAST(SHA256($2) AS VARCHAR(100))',
            [reqLogin.name, reqLogin.password]);
        const resLogin: Login = queryRes.rows[0];
        if (resLogin) {
            res.status(200).json(new Response(200, 'Logged in'));
            return;
        }
    }
    res.status(401).send(new Response(401, 'Unauthorized'));
});

// Starting
const port = 3000;
app.listen(port, () => {
    console.log(`App listening on port ${port}...`);
    pgClient.connect();
});

// Shutdown event-handling
for (let signal of ['SIGINT', 'SIGTERM']) {
    process.on(signal, () => {
        console.log('Application shutdown');
        pgClient.end();
        process.exit();
    });
}