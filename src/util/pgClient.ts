import { Client } from "pg";
import dbClient from '../config/db-client.json';

export default new Client(dbClient);