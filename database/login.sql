/* Login */

/* Table */
CREATE TABLE login (
    name VARCHAR(25) PRIMARY KEY,
    password VARCHAR(100) NOT NULL
);

/* Default */
INSERT INTO login (name, password) VALUES ('mynotes', SHA256('mynotes'));