/* Execute with role "psql -U postgres -f database/init.sql" */

/* Conditional drops */
DROP DATABASE IF EXISTS mynotes;
DROP ROLE IF EXISTS mynotes;

/* Role */
CREATE ROLE mynotes WITH LOGIN  ENCRYPTED PASSWORD 'mynotes' CREATEDB SUPERUSER;

/* Database */
CREATE DATABASE mynotes OWNER mynotes;

/* Privileges */
GRANT ALL PRIVILEGES ON DATABASE mynotes to mynotes;

/* Connect */
\c postgresql://mynotes:mynotes@localhost:5432;

/* Tables */
\ir login.sql
\ir note.sql