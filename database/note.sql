/* Note */

/* Table */
CREATE TABLE note (
    date DATE DEFAULT NOW()::DATE,
    text VARCHAR(1000),
    login_name VARCHAR(25) REFERENCES login (name),
    PRIMARY KEY (date, login_name)
);